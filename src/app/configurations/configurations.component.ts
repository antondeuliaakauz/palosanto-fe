import { Component, OnInit } from '@angular/core';
import { ProductsComponent } from '../products/products.component';
import { ConfigurationsService } from '../services/configurations.service';
import { IConfiguration } from '../../interfaces/configuration.interface';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-configurations',
  standalone: true,
  imports: [ProductsComponent, FormsModule],
  templateUrl: './configurations.component.html',
  styleUrl: './configurations.component.scss',
})
export class ConfigurationsComponent implements OnInit {
  constructor(
    public readonly productsComponent: ProductsComponent,
    private readonly configurationsService: ConfigurationsService
  ) {}

  configuration: IConfiguration | null = null;

  formData = {
    defaultBudget: this.configuration?.defaultBudget,
    defaultCurrency: this.configuration?.defaultCurrency,
  };

  ngOnInit(): void {
    this.get();

    this.formData = {
      defaultBudget: this.configuration?.defaultBudget,
      defaultCurrency: this.configuration?.defaultCurrency,
    };
  }

  get() {
    this.configurationsService
      .get()
      .subscribe((configuration: IConfiguration) => {
        this.configuration = configuration;
      });
  }

  updateOne(
    id: number,
    data: { safeDelete?: boolean; closeAfterCreate?: boolean }
  ) {
    this.configurationsService
      .updateOne(id, data)
      .subscribe((configuration: IConfiguration) => {
        this.configuration = configuration;
        this.productsComponent.configuration = configuration;
      });
  }
}
