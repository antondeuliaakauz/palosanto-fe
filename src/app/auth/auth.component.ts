import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { RegisterForm } from './types/register-form.type';
import { LoginForm } from './types/login-form.type';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { catchError, tap, throwError } from 'rxjs';
import { Router, RouterModule } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthMode } from '../../utils/enums/auth-mode.enum';

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [FormsModule, RouterModule, ReactiveFormsModule],
  providers: [AuthService],
  templateUrl: './auth.component.html',
  styleUrl: './auth.component.scss',
})
export class AuthComponent implements OnInit {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly toastrService: ToastrService,
    private readonly fb: FormBuilder
  ) {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(6)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.loginForm = this.fb.group({
      emailOrUsername: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  registerForm: FormGroup;
  loginForm: FormGroup;

  authMode: 'signUp' | 'signIn' = AuthMode.SIGN_UP;

  changeAuthMode(mode: 'signUp' | 'signIn') {
    this.authMode = mode;

    localStorage.setItem('authMode', mode);
  }

  ngOnInit(): void {
    const authMode = localStorage.getItem('authMode') as 'signUp' | 'signIn';

    if (authMode) this.authMode = authMode;
  }

  errorMessage = {
    show: false,
    message: '',
  };

  register() {
    this.errorMessage = {
      show: false,
      message: '',
    };

    if (this.registerForm.valid) {
      this.authService
        .register(this.registerForm.value)
        .pipe(
          tap((data: { accessToken: string }) => {
            this.showSuccess({
              title: 'Success',
              message: 'Account created successfully',
            });

            localStorage.setItem('accessToken', data.accessToken);

            this.router.navigate(['/products/items']);
          }),
          catchError((error) => {
            this.showError({ title: 'Error', message: error.error.message });

            return throwError(() => error);
          })
        )
        .subscribe();
    } else {
      this.errorMessage = {
        show: true,
        message: 'Please type all the requied fields',
      };

      this.showError({
        title: 'Error',
        message: 'Please type all the required fields',
      });
    }
  }

  login() {
    if (this.loginForm.valid) {
      this.authService
        .login(this.loginForm.value)
        .pipe(
          tap((data: { accessToken: string }) => {
            this.showSuccess({
              title: 'Success',
              message: 'Logined successfully',
            });

            localStorage.setItem('accessToken', data.accessToken);

            this.router.navigate(['/products/items']);
          }),
          catchError((error) => {
            this.showError({ title: 'Error', message: error.error.message });

            return throwError(() => error);
          })
        )
        .subscribe();
    }
  }

  logout() {
    this.authService.logout().subscribe(() => {});
  }

  // Toastr
  showSuccess({ title, message }: { title: string; message: string }) {
    this.toastrService.success(message, title, {
      positionClass: 'toast-top-right',
      closeButton: true,
      timeOut: 2500,
      progressBar: true,
    });
  }

  showError({ title, message }: { title: string; message: string }) {
    this.toastrService.error(message, title, {
      positionClass: 'toast-top-right',
      closeButton: true,
      timeOut: 2500,
      progressBar: true,
    });
  }
}
