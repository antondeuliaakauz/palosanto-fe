export type LoginForm = {
  emailOrUsername: string;

  password: string;
};
