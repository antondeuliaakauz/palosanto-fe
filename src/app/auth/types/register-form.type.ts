export type RegisterForm = {
  email: string;

  username: string;

  password: string;
};
