import { Component, OnInit } from '@angular/core'
import { Router, RouterLink, RouterModule } from '@angular/router'
import { IProduct } from '../../interfaces/product.interface'
import { ProductsService } from '../services/products.service'
import { HttpClient } from '@angular/common/http'
import {
	FormBuilder,
	FormGroup,
	FormsModule,
	ReactiveFormsModule,
	Validators
} from '@angular/forms'
import { ConfigurationsComponent } from '../configurations/configurations.component'
import { IConfiguration } from '../../interfaces/configuration.interface'
import { ConfigurationsService } from '../services/configurations.service'
import { TableComponent } from '../table/table.component'
import { backendUrl } from '../../utils/constants'
import { ToastrService } from 'ngx-toastr'
import { CommonModule } from '@angular/common'
import { catchError, pipe, tap, throwError } from 'rxjs'
import { ItemsComponent } from '../items/items.component'
import { IUser } from '../../interfaces/user.interface'
import { UsersService } from '../services/users.service'
import { AuthService } from '../services/auth.service'

@Component({
	selector: 'app-products',
	standalone: true,
	imports: [
		RouterLink,
		FormsModule,
		ConfigurationsComponent,
		TableComponent,
		RouterModule,
		CommonModule,
		ReactiveFormsModule
	],
	providers: [ConfigurationsService, UsersService, AuthService],
	templateUrl: './products.component.html',
	styleUrl: './products.component.scss'
})
export class ProductsComponent implements OnInit {
	constructor(
		private readonly productsService: ProductsService,
		private readonly configurationsService: ConfigurationsService,
		private readonly toastr: ToastrService,
		private readonly router: Router,
		private readonly usersService: UsersService,
		private readonly authService: AuthService,
		private readonly fb: FormBuilder
	) {
		this.createOneForm = fb.group({
			name: ['', Validators.required],
			price: ['', Validators.required],
			buyer: ['0', Validators.maxLength(3)]
		})
	}

	showSuccess({ title, message }: { title: string; message: string }) {
		this.toastr.success(message, title, {
			positionClass: 'toast-top-right',
			closeButton: true,
			timeOut: 2500,
			easeTime: 1000,
			easing: 'ease-in',
			progressBar: true
		})
	}

	configuration: IConfiguration | null = null

	createOneForm: FormGroup

	budget: number = 0

	createWindowOpen = false

	user: IUser | null = null

	getUser() {
		this.usersService.getOne().subscribe((user: IUser) => (this.user = user))
	}

	onSubmit() {
		if (this.createOneForm.valid) {
			this.productsService
				.createOne(this.createOneForm.value)
				.pipe(
					tap(() => {
						if (this.configuration?.closeAfterCreate) {
							this.createWindowOpen = false
						}

						this.get(this.budget)
					}),
					catchError(error => {
						return throwError(error)
					})
				)
				.subscribe()

			this.showSuccess({ title: 'Success', message: 'The item was created' })

			this.errorEmptyValues = false
		} else {
			this.errorEmptyValues = true
		}
	}

	getByBudget() {
		this.get(this.budget)
	}

	public errorEmptyValues: boolean = false

	public products: IProduct[] = []

	isCurrentRoute(route: string): boolean {
		return this.router.url.includes(route)
	}

	logout() {
		this.authService.logout().subscribe(() => {
			this.router.navigate(['/auth'])

			localStorage.removeItem('accessToken')
		})
	}

	ngOnInit(): void {
		this.getUser()

		this.getConfiguration()
	}

	getConfiguration() {
		this.configurationsService.get().subscribe((configuration: IConfiguration) => {
			this.budget = configuration.defaultBudget

			this.get(this.budget)

			this.configuration = configuration
		})
	}

	get(budget: number) {
		this.productsService.get(budget).subscribe((products: IProduct[]) => {
			this.products = products
		})
	}

	warningWindow: boolean = false

	toggleWarning({ product, state }: { product?: IProduct; state: boolean }) {
		this.warningWindow = state

		if (product) {
			this.windowItem = product
		}
	}

	windowItem: IProduct | null = null

	deleteOne(id: number) {
		this.productsService.deleteOne(id).subscribe(() => {
			this.get(this.budget)

			this.showSuccess({ title: 'Success', message: 'The item was deleted' })

			// this.selectedItemIds.filter((itemId) => itemId !== id);
		})

		this.warningWindow = false
	}

	// Settings
	settingsOpen: boolean = false

	toggleSettings(state: boolean) {
		this.settingsOpen = state
	}

	// Currencies
	currenciesOpen: boolean = false

	toggleCurrencies(state: boolean) {
		this.currenciesOpen = state
	}

	// Buyers
	buyersOpen: boolean = false

	toggleBuyers(state: boolean) {
		this.buyersOpen = state
	}
}
