import { Component } from '@angular/core'
import { ProductsComponent } from '../products/products.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { TableComponent } from '../table/table.component'
import { ToastrService } from 'ngx-toastr'
import { ProductsService } from '../services/products.service'
import { IProduct } from '../../interfaces/product.interface'
import { ConfigurationsService } from '../services/configurations.service'

@Component({
	selector: 'app-items',
	standalone: true,
	imports: [ProductsComponent, FormsModule, TableComponent, ReactiveFormsModule],
	templateUrl: './items.component.html',
	styleUrl: './items.component.scss'
})
export class ItemsComponent {
	constructor(
		public readonly productsComponent: ProductsComponent,
		private readonly toastr: ToastrService,
		private readonly productsService: ProductsService,
		private readonly configurationsService: ConfigurationsService
	) {}

	// Temporary
	selectedItems: IProduct[] = []

	data = {
		budgetBeforeProfit: 0,
		budgetAfterProfit: 0,
		totalProfit: 0,
		totalPrice: 0
	}

	changeBudget({ amount, operator }: { amount: number; operator: '+' | '-' }) {
		switch (operator) {
			case '+':
				this.productsComponent.budget += amount

				if (this.productsComponent.configuration) {
					this.configurationsService
						.updateOne(this.productsComponent.configuration?.id, {
							defaultBudget: this.productsComponent.budget
						})
						.subscribe(() => {
							this.productsComponent.getConfiguration()
							this.getSelected(this.selectedItemIds)
						})
				}
				break
			case '-':
				this.productsComponent.budget -= amount

				if (this.productsComponent.configuration) {
					this.configurationsService
						.updateOne(this.productsComponent.configuration?.id, {
							defaultBudget: this.productsComponent.budget
						})
						.subscribe(() => {
							this.productsComponent.getConfiguration()
							this.getSelected(this.selectedItemIds)
						})
				}
				break
		}
	}

	addDuplicate(product: IProduct) {
		this.selectedItemIds.push(product.id)

		this.productsService
			.updateOne(product.id, { amount: product.amount + 1 })
			.subscribe(() => {
				this.getSelected(this.selectedItemIds)
			})
	}

	removeDuplicate(product: IProduct) {
		if (product.amount === 1) {
			console.log('first')
			const without = this.selectedItemIds.filter(itemId => itemId !== product.id)

			this.selectedItemIds = without

			this.getSelected(this.selectedItemIds)
		} else {
			console.log('second')
			const index = this.selectedItemIds.lastIndexOf(product.id)

			if (index !== -1) {
				this.selectedItemIds.splice(index, 1)
			}

			this.productsService
				.updateOne(product.id, { amount: product.amount - 1 })
				.subscribe(() => this.getSelected(this.selectedItemIds))
		}
	}

	getAmount(id: number) {
		const selectedItemIds = this.selectedItemIds.filter(
			selectedItemId => selectedItemId === id
		)

		return selectedItemIds.length
	}

	getSelected(selectedItemIdsToFind: number[]) {
		this.productsService
			.getSelected({
				itemIds: selectedItemIdsToFind,
				budget: this.productsComponent.budget
			})
			.subscribe(
				(products: {
					items: IProduct[]
					budgetBeforeProfit: number
					budgetAfterProfit: number
					totalProfit: number
					totalPrice: number
				}) => {
					this.selectedItems = products.items
					this.data.budgetBeforeProfit = products.budgetBeforeProfit
					this.data.budgetAfterProfit = products.budgetAfterProfit
					this.data.totalPrice = products.totalPrice
					this.data.totalProfit = products.totalProfit
				}
			)
	}

	selectItem(id: number) {
		if (this.selectedItemIds.includes(id)) {
			this.selectedItemIds = this.selectedItemIds.filter(itemId => itemId !== id)

			const without = this.selectedItemIds.filter(itemId => itemId !== id)

			this.selectedItemIds = without

			this.getSelected(this.selectedItemIds)
		} else {
			this.selectedItemIds.push(id)

			this.getSelected(this.selectedItemIds)
		}
	}

	selectAll() {
		if (this.productsComponent.products.length) {
			for (const product of this.productsComponent.products) {
				if (!this.selectedItemIds.includes(product.id)) {
					this.selectedItemIds.push(product.id)
				}
			}
		}
	}

	cancelAll() {
		this.selectedItemIds = []
	}

	selectedItemIds: number[] = []
	// Temporary

	toggleCreateWindow(state: boolean) {
		this.productsComponent.createWindowOpen = state
	}

	showSuccess({ title, message }: { title: string; message: string }) {
		this.toastr.success(message, title, {
			positionClass: 'toast-top-right',
			closeButton: true,
			timeOut: 2500,
			easeTime: 1000,
			easing: 'ease-in',
			progressBar: true
		})
	}
}
