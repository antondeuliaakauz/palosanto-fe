import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { backendUrl } from '../../utils/constants';
import { Observable } from 'rxjs';
import { IConfiguration } from '../../interfaces/configuration.interface';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationsService {
  constructor(private readonly http: HttpClient) {}

  private readonly url: string = `${backendUrl}configurations`;

  get(): Observable<IConfiguration> {
    return this.http.get(this.url, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IConfiguration>;
  }

  createOne(data: { displayName: string; percent: number }) {
    const token = localStorage.getItem('accessToken');

    console.log(token);

    return this.http.post(this.url, data, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }

  updateOne(
    id: number,
    data: {
      safeDelete?: boolean;
      closeAfterCreate?: boolean;
      defaultBudget?: number;
    }
  ): Observable<IConfiguration> {
    return this.http.patch(`${this.url}/${id}`, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IConfiguration>;
  }

  deleteOne() {
    return this.http.delete(this.url, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    });
  }
}
