import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { backendUrl } from '../../utils/constants'
import { IUser } from '../../interfaces/user.interface'

@Injectable()
export class UsersService {
	private readonly url = `${backendUrl}users`

	constructor(private readonly httpClient: HttpClient) {}

	getOne(): Observable<IUser> {
		return this.httpClient.get(this.url, {
			headers: {
				Authorization: `Bearer ${this.getAccessToken()}`
			}
		}) as Observable<IUser>
	}

	updateOne(
		id: number,
		dto: { email: string; username: string; password: string }
	): Observable<IUser> {
		return this.httpClient.patch(`${this.url}/${id}`, dto, {
			headers: {
				Authorization: `Bearer ${this.getAccessToken()}`
			}
		}) as Observable<IUser>
	}

	logout() {
		return this.httpClient.post(this.url, {})
	}

	getAccessToken() {
		return localStorage.getItem('accessToken')
	}
}
