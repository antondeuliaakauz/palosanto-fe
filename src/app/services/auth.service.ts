import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { backendUrl } from '../../utils/constants'
import { Observable } from 'rxjs'

@Injectable()
export class AuthService {
	private readonly url = `${backendUrl}auth`

	constructor(private readonly http: HttpClient) {}

	register(data: {
		email: string
		username: string
		password: string
	}): Observable<{ accessToken: string }> {
		return this.http.post(`${this.url}/register`, data) as Observable<{
			accessToken: string
		}>
	}

	login(data: {
		emailOrUsername: string
		password: string
	}): Observable<{ accessToken: string }> {
		return this.http.post(`${this.url}/login`, data) as Observable<{
			accessToken: string
		}>
	}

	logout(): Observable<{}> {
		return this.http.delete(`${this.url}/logout`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		}) as Observable<{}>
	}
}
