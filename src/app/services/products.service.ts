import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IProduct } from '../../interfaces/product.interface';
import { Observable } from 'rxjs';
import { backendUrl } from '../../utils/constants';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private readonly url = `${backendUrl}items/`;

  constructor(private readonly httpClient: HttpClient) {}

  createOne(data: any): Observable<IProduct> {
    return this.httpClient.post(`${backendUrl}items`, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IProduct>;
  }

  get(budget: number): Observable<IProduct[]> {
    let url = '';

    if (budget) {
      url = `${this.url}?budget=${budget}`;
    } else {
      url = this.url;
    }

    return this.httpClient.get(url, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IProduct[]>;
  }

  deleteOne(id: number): Observable<IProduct> {
    console.log(`${this.url}${id}`);
    return this.httpClient.delete(`${this.url}${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IProduct>;
  }

  getSelected({
    itemIds,
    budget,
  }: {
    itemIds: number[];
    budget: number;
  }): Observable<{
    items: IProduct[];
    budgetBeforeProfit: number;
    budgetAfterProfit: number;
    totalProfit: number;
    totalPrice: number;
  }> {
    const res = this.httpClient.post(
      `${this.url}selected`,
      {
        itemIds,
        budget,
      },
      {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
        },
      }
    ) as Observable<{
      items: IProduct[];
      budgetBeforeProfit: number;
      budgetAfterProfit: number;
      totalProfit: number;
      totalPrice: number;
    }>;

    return res;
  }

  updateOne(id: number, data: { amount: number }): Observable<IProduct> {
    return this.httpClient.patch(`${this.url}${id}`, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
      },
    }) as Observable<IProduct>;
  }
}
