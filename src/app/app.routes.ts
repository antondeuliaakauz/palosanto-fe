import { Routes } from '@angular/router'
import { ProductsComponent } from './products/products.component'
import { ItemsComponent } from './items/items.component'
import { AuthComponent } from './auth/auth.component'
import { JwtGuard } from './guards/jwt.guard'
import { AuthGuard } from './guards/auth.guard'
import { ConfigurationsComponent } from './configurations/configurations.component'

export const routes: Routes = [
	{
		path: 'auth',
		component: AuthComponent,
		canActivate: [AuthGuard]
	},
	{
		path: 'products',
		component: ProductsComponent,
		canActivate: [JwtGuard],
		children: [
			{
				path: 'items',
				component: ItemsComponent,
				canActivate: [JwtGuard]
			},
			{
				path: 'settings',
				component: ConfigurationsComponent,
				canActivate: [JwtGuard]
			}
		]
	},
	{
		path: '**',
		redirectTo: '/products/items'
	}
]
