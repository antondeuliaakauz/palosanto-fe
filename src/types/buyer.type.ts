export type Buyer = {
  displayName: string;

  percent: number;
};
