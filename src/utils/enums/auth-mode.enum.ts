export enum AuthMode {
  SIGN_UP = 'signUp',
  SIGN_IN = 'signIn',
}
