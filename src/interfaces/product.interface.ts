export interface IProduct {
  id: number;

  name: string;

  price: number;

  buyer: number;

  profitPercent: number;

  profit: number;

  budgetBeforeProfit: number;

  budgetAfterProfit: number;

  accessable: boolean;

  createdAt: Date;

  updatedAt: Date;

  amount: number;
}
