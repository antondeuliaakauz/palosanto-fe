export interface ICurrency {
  id: number;

  displayName: string;

  symbol?: string;

  createdAt: Date;

  updatedAt: Date;
}
