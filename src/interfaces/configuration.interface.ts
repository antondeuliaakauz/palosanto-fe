export interface IConfiguration {
  id: number;

  defaultBudget: number;

  defaultCurrency: string;

  safeDelete: boolean;

  closeAfterCreate: boolean;
}
