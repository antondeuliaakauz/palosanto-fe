export interface IBuyer {
  id: number;

  displayName: string;

  percent: number;

  createdAt: Date;

  updatedAt: Date;
}
